# PlayTime

It's a plugin for a [Spigot](https://www.spigotmc.org) Minecraft server to log the time players spend on the server and returns it to them.

## Features

- Simple usage
- No external DB needed
- No config
- Plug and Play
- Scheduled saving to prevent data loses by a crash
- Written in [Kotlin](https://kotlinlang.org/)

## Commands

`uptime` shows the current up time of the server. It's inspired by the linux tool returning the systems up time in DD:HH:MM format.

`playtime` shows the total play time on the server (Format DD:HH:MM)

## Development

The plugin gets build with gradle. It is important to run the shadowJar task to include all dependencies.
It's entirely written in Kotlin and for testing it uses [Mockk](https://mockk.io/) and [Junit5](https://junit.org/junit5/).

This plugin uses the [linter from Pinterest for Kotlin](https://www.kotlinresources.com/library/ktlint/)

If you want to contribute make sure to include the test for your code and add [Kotlin doc](https://kotlinlang.org/docs/reference/kotlin-doc.html).

**Feel free to create a merge request!**

