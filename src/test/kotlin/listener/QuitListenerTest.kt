package listener

import PlayerService
import io.mockk.Runs
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.util.UUID
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerQuitEvent
import org.junit.jupiter.api.Test

class QuitListenerTest {

    @Test
    fun `should call to add a player`() {
        val mockPlayer = mockk<Player>()
        every { mockPlayer.uniqueId } returns UUID.randomUUID()
        every { mockPlayer.name } returns ""

        val mockPlayerQuitEvent = mockk<PlayerQuitEvent>()
        every { mockPlayerQuitEvent.player } returns mockPlayer

        val mockPlayerService = mockk<PlayerService>()
        every { mockPlayerService.removePlayer(any()) } just Runs

        val quitListener = QuitListener(mockPlayerService)
        quitListener.onPlayerQuit(mockPlayerQuitEvent)

        verify(exactly = 1) { mockPlayerService.removePlayer(any()) }
        confirmVerified(mockPlayerService)
    }
}
