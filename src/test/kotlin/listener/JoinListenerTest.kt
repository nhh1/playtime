package listener

import PlayerService
import io.mockk.Runs
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.util.UUID
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerJoinEvent
import org.junit.jupiter.api.Test

class JoinListenerTest {

    @Test
    fun `should call to add a player`() {
        val mockPlayer = mockk<Player>()
        every { mockPlayer.uniqueId } returns UUID.randomUUID()
        every { mockPlayer.name } returns ""

        val mockPlayerJoinEvent = mockk<PlayerJoinEvent>()
        every { mockPlayerJoinEvent.player } returns mockPlayer

        val mockPlayerService = mockk<PlayerService>()
        every { mockPlayerService.addPlayer(any(), any()) } just Runs

        val joinListener = JoinListener(mockPlayerService)
        joinListener.onPlayerJoin(mockPlayerJoinEvent)

        verify(exactly = 1) { mockPlayerService.addPlayer(any(), any()) }
        confirmVerified(mockPlayerService)
    }
}
