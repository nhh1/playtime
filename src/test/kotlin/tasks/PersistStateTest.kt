package tasks

import PlayerService
import io.mockk.Runs
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import kotlin.test.Test

class PersistStateTest {

    @Test
    fun `should persist all players`() {
        val mockPlayerService = mockk<PlayerService>()
        every { mockPlayerService.persistAllPlayer() } just Runs

        val persistState = PersistState(mockPlayerService)
        persistState.run()

        verify(exactly = 1) { mockPlayerService.persistAllPlayer() }
        confirmVerified(mockPlayerService)
    }
}
