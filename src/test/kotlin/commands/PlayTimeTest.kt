package commands

import PlayerService
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import java.time.Duration
import java.util.UUID
import kotlin.test.assertTrue
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

internal class PlayTimeTest {

    @Test
    fun `get the current play time`() {
        val list = mutableListOf<String>()

        val mockedPlayerService = mockk<PlayerService>()
        every { mockedPlayerService.timePlayed(any()) } returns Duration.ofMinutes(10)

        val playTime = PlayTime(mockedPlayerService)

        val mockCommandSender = mockk<Player>()
        every { mockCommandSender.uniqueId } returns UUID.randomUUID()
        every { mockCommandSender.sendMessage(capture(list)) } just Runs

        assertTrue(playTime.onCommand(mockCommandSender, mockk(), "", arrayOf()))

        assertFalse(list.isEmpty())
        assertEquals("Your play time: 00:00:10", list[0])
    }

    @Test
    fun `error if not send by player`() {
        val list = mutableListOf<String>()

        val mockedPlayerService = mockk<PlayerService>()
        every { mockedPlayerService.timePlayed(any()) } returns Duration.ofMinutes(10)

        val playTime = PlayTime(mockedPlayerService)

        val mockCommandSender = mockk<CommandSender>()
        every { mockCommandSender.sendMessage(capture(list)) } just Runs

        assertFalse(playTime.onCommand(mockCommandSender, mockk(), "", arrayOf()))

        assertFalse(list.isEmpty())
        assertEquals("This command can only be executed as a player", list[0])
    }
}
