import db.PlayerTimeDB
import io.mockk.Runs
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.time.Duration
import java.time.LocalDateTime
import java.util.UUID
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import models.Player
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class PlayerServiceTest {

    @Nested
    inner class AddPlayerTest {
        @Test
        fun `should add new player`() {
            val uuid = UUID.randomUUID()

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(uuid) } returns false
            every { mockDB.createPlayer(any()) } just Runs

            val playerService = PlayerService(mockDB)
            assertTrue(playerService.isEmpty())

            playerService.addPlayer(uuid, "playerName")

            assertFalse(playerService.isEmpty())
            assertTrue(playerService.containsPlayer(uuid))

            verify(exactly = 1) { mockDB.findByID(uuid) }
            verify(exactly = 1) { mockDB.createPlayer(any()) }
            confirmVerified(mockDB)
        }

        @Test
        fun `should add existing player`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns player

            val playerService = PlayerService(mockDB)

            assertTrue(playerService.isEmpty())
            playerService.addPlayer(uuid, "playerName")

            assertFalse(playerService.isEmpty())
            assertTrue(playerService.containsPlayer(uuid))

            verify(exactly = 1) { mockDB.findByID(any()) }
            verify(exactly = 1) { mockDB.readPlayer(any()) }
            confirmVerified(mockDB)
        }

        @Test
        fun `should change join time by existing player`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")
            player.joinTime = LocalDateTime.now().minusDays(1)
            val originalPlayer = player.copy()

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns player

            val playerService = PlayerService(mockDB)

            playerService.addPlayer(uuid, "playerName")

            val extract = playerService.getPlayer(uuid)

            assertNotEquals(originalPlayer.joinTime, extract.joinTime)
        }
    }

    @Test
    fun `remove a player from the map`() {
        val uuid = UUID.randomUUID()
        val player = Player(uuid, "playerName")

        val mockDB = mockk<PlayerTimeDB>()
        every { mockDB.findByID(any()) } returns true
        every { mockDB.readPlayer(any()) } returns player
        every { mockDB.writePlayer(any()) } just Runs

        val playerService = PlayerService(mockDB)
        playerService.addPlayer(uuid, "playerName")

        assertFalse(playerService.isEmpty())

        playerService.removePlayer(uuid)

        assertTrue(playerService.isEmpty())
    }

    @Nested
    inner class PersistPlayerTest {

        @Test
        fun `persist a player from the map to the db`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns player
            every { mockDB.writePlayer(any()) } just Runs

            val playerService = PlayerService(mockDB)
            playerService.addPlayer(uuid, "playerName")

            playerService.persistPlayer(uuid)

            verify(exactly = 1) { mockDB.writePlayer(any()) }
        }

        @Test
        fun `persist a player from the map to the db with updated playtime`() {
            val captureList = mutableListOf<Player>()
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns player
            every { mockDB.writePlayer(capture(captureList)) } just Runs

            val playerService = PlayerService(mockDB)
            playerService.addPlayer(uuid, "playerName")

            playerService.persistPlayer(uuid)

            assertEquals(1, captureList.size)
            assertNotEquals(Duration.ZERO, captureList.first().playTime)

            verify(exactly = 1) { mockDB.writePlayer(any()) }
        }

        @Test
        fun `persist all player from the map`() {
            val uuid = UUID.randomUUID()
            val uuid2 = UUID.randomUUID()

            val player = Player(uuid, "playerName")
            val player2 = Player(uuid, "playerName")

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(uuid) } returns player
            every { mockDB.readPlayer(uuid2) } returns player2
            every { mockDB.writePlayer(any()) } just Runs

            val playerService = PlayerService(mockDB)
            playerService.addPlayer(uuid, "playerName")
            playerService.addPlayer(uuid2, "playerName")

            assertFalse(playerService.isEmpty())

            playerService.persistAllPlayer()

            verify(exactly = 2) { mockDB.writePlayer(any()) }
        }
    }

    @Nested
    inner class GetPlayerTest {

        @Test
        fun `get player`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns player

            val playerService = PlayerService(mockDB)

            playerService.addPlayer(uuid, "playerName")

            assertFalse(playerService.isEmpty())

            val extractedPlayer = playerService.getPlayer(uuid)

            assertEquals(player.uuid, extractedPlayer.uuid)
            assertEquals(player.playerName, extractedPlayer.playerName)
            assertEquals(player.playTime, extractedPlayer.playTime)
        }
    }

    @Nested
    inner class TimePlayedTest {

        @Test
        fun `get played time and update the field`() {
            val uuid = UUID.randomUUID()
            val player = Player(uuid, "playerName")

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns player

            val playerService = PlayerService(mockDB)

            playerService.addPlayer(uuid, "playerName")

            assertFalse(playerService.isEmpty())

            val playTime = playerService.timePlayed(uuid)

            assertNotEquals(Duration.ZERO, playTime)
            assertFalse(playTime.isNegative)
            assertEquals(Duration.ZERO, playerService.getPlayer(uuid).playTime)
        }

        @Test
        fun `get played time after the time was saved`() {
            val uuid = UUID.randomUUID()
            val mockPlayer = Player(uuid, "playerName", lastSave = LocalDateTime.now().plusMinutes(10))

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns mockPlayer

            val playerService = PlayerService(mockDB)

            playerService.addPlayer(uuid, "playerName")

            assertFalse(playerService.isEmpty())

            val playTime = playerService.timePlayed(uuid)

            assertTrue(playTime.toMinutes() <= -9, "the result is negative because it is a synthetic test")
            println(playTime.toMinutes())
        }

        @Test
        fun `get played time from the join when the save time earlier than the join time`() {
            val uuid = UUID.randomUUID()
            val mockPlayer = Player(uuid, "playerName", lastSave = LocalDateTime.now().minusMinutes(10))

            val mockDB = mockk<PlayerTimeDB>()
            every { mockDB.findByID(any()) } returns true
            every { mockDB.readPlayer(any()) } returns mockPlayer

            val playerService = PlayerService(mockDB)

            playerService.addPlayer(uuid, "playerName")

            assertFalse(playerService.isEmpty())

            val playTime = playerService.timePlayed(uuid)

            assertNotEquals(Duration.ZERO, playTime)
            assertFalse(playTime.isNegative)
            println(playTime.toMinutes())
        }
    }
}
