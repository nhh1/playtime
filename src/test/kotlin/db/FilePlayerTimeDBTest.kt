package db

import com.google.gson.Gson
import java.io.File
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import models.Player
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue

internal class FilePlayerTimeDBTest {

    private var dbDir = createTempDir()

    @BeforeTest
    fun setUp() {
        dbDir = createTempDir()
    }

    @AfterTest
    fun cleanUp() {
        dbDir.delete()
    }

    @Test
    fun `find user over uuid file name`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)

        assertFalse(fileDB.findByID(uuid))

        File(dbDir, uuid.toString()).createNewFile()
        assertTrue(fileDB.findByID(uuid))
    }

    @Test
    fun `create new player file`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)
        val player = Player(uuid, "username")

        assertFalse(fileDB.findByID(uuid))

        fileDB.createPlayer(player)

        assertTrue(fileDB.findByID(uuid))

        val writtenFile = File(dbDir, uuid.toString())
        assertEquals(player, Gson().fromJson(writtenFile.readText(), Player::class.java))
    }

    @Test
    fun `read player from file`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)
        val player = Player(uuid, "username")

        File(dbDir, uuid.toString()).writeText(Gson().toJson(player))

        assertEquals(player, fileDB.readPlayer(uuid))
    }

    @Test
    fun `write player to file`() {
        val uuid = UUID.randomUUID()
        val fileDB = FilePlayerTimeDB(dbDir)
        val player = Player(uuid, "username")

        val file = File(dbDir, uuid.toString())
        assertTrue(file.createNewFile())
        fileDB.writePlayer(player)
        assertEquals(player, Gson().fromJson(file.readText(), Player::class.java))
    }
}
