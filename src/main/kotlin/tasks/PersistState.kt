package tasks

import PlayerService

class PersistState(private val playerService: PlayerService) : Runnable {

    override fun run() {
        playerService.persistAllPlayer()
    }
}
