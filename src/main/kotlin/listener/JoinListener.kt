package listener

import PlayerService
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

/**
 * The listener for the PlayerJoinEvent
 *
 * @property playerService the playService to add player to it
 */
class JoinListener(private val playerService: PlayerService) : Listener {

    /**
     * Registers as handler for the PlayerJoinEvent
     * Adds the player to the service
     *
     * @param event to get the player that emitted it
     */
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        playerService.addPlayer(player.uniqueId, player.name)
    }
}
