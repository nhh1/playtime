package listener

import PlayerService
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent

/**
 * The listener for the PlayerQuitEvent
 *
 * @property playerService the playService to remove player from it
 */
class QuitListener(private val playerService: PlayerService) : Listener {

    /**
     * Registers as handler for the PlayerQuitEvent
     * Removes the user from the service
     *
     * @param event to get the player that emitted it
     */
    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val player = event.player
        playerService.removePlayer(player.uniqueId)
    }
}
