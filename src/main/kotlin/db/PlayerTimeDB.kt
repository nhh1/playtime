package db

import java.util.UUID
import models.Player

interface PlayerTimeDB {

    /**
     * It should look if a player exists
     *
     * @param uuid the id of the player
     *
     * @return if the file is present
     */
    fun findByID(uuid: UUID): Boolean

    /**
     * Should create a player record for persisting
     *
     * @param player the player that should be save
     */
    fun createPlayer(player: Player)

    /**
     * Should read the player
     *
     * @param uuid the uuid of the player that should be read
     *
     * @return the actual player
     */
    fun readPlayer(uuid: UUID): Player

    /**
     * Should write a player into persistence
     * To create a new player file @see createPlayer(player: Player)
     *
     * @param player that should be written
     */
    fun writePlayer(player: Player)
}
