package db

import com.google.gson.Gson
import java.io.File
import java.util.UUID
import models.Player

/**
 * FileDB manages the file based PlayerTime DB and provides basic functionality to interact with it
 *
 * @property path optional path for the storage of the data
 * @property gson gson object to work with json
 */
class FilePlayerTimeDB(private val path: File = File("./plugins/PlayTime/")) : PlayerTimeDB {

    private val gson = Gson()

    /**
     * Ensures that the directory exists
     */
    init {
        path.mkdirs()
    }

    /**
     * It looks if a certain file already exits
     *
     * @param uuid the id for the file that should be found
     *
     * @return if the file is present
     */
    override fun findByID(uuid: UUID): Boolean {
        return File(path, uuid.toString()).exists()
    }

    /**
     * Creates a player file
     *
     * @param player the player that should be saved to the new file
     *
     */
    override fun createPlayer(player: Player) {
        val file = File(path, player.uuid.toString())
        if (!file.exists()) file.createNewFile()
        writePlayer(player)
    }

    /**
     * Reads the player out of a file
     *
     * @param uuid the id of the player that should be read
     *
     * @return Player
     */
    override fun readPlayer(uuid: UUID): Player {
        val file = File(path, uuid.toString())
        return gson.fromJson(file.readText(), Player::class.java)
    }

    /**
     * Writs a player into an existing file
     * To create a new player file @see createPlayer(player: Player)
     *
     * @param player that should be written
     */
    override fun writePlayer(player: Player) {
        val file = File(path, player.uuid.toString())
        file.writeText(gson.toJson(player))
    }
}
