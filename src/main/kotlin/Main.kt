import commands.PlayTime
import commands.TestCommand
import commands.UpTime
import db.FilePlayerTimeDB
import listener.JoinListener
import listener.QuitListener
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitScheduler
import tasks.PersistState

/**
 * Entrypoint of the plugin
 *
 * @property playerService A singleton of the playerService with FilePlayerTimeDB
 * @property scheduler the scheduler to manage schedules
 */
class Main : JavaPlugin() {

    private val playerService = PlayerService(FilePlayerTimeDB())
    private val scheduler: BukkitScheduler = server.scheduler

    /**
     * Gets executed when ever the server starts
     * Sets the command executors and registers the event handlers
     * Sets schedule tasks
     */
    override fun onEnable() {
        super.onEnable()

        this.getCommand("test")?.setExecutor(TestCommand())
        this.getCommand("playtime")?.setExecutor(PlayTime(playerService))
        this.getCommand("uptime")?.setExecutor(UpTime())

        server.pluginManager.registerEvents(JoinListener(playerService), this)
        server.pluginManager.registerEvents(QuitListener(playerService), this)

        scheduler.scheduleSyncRepeatingTask(this, PersistState(playerService), 0, minutesToTicks(10))
    }

    /**
     * Gets executed when ever the server gets properly shutdown
     * It saves all stats to the db
     */
    override fun onDisable() {
        super.onDisable()
        playerService.persistAllPlayer()
    }

    private fun secondsToTicks(seconds: Int): Long {
        return (seconds * 20).toLong()
    }

    private fun minutesToTicks(minutes: Int): Long {
        val seconds = minutes * 60
        return secondsToTicks(seconds)
    }
}
