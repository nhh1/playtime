import db.PlayerTimeDB
import java.time.Duration
import java.time.LocalDateTime
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import models.Player

/**
 * A service to hold the all player that are currently playing
 * It should only be used as Singleton @see Kotlin objects
 *
 * @property db the DB implementation which should be used to persist the state
 * @property playerMap Holds all online players
 */
open class PlayerService(private val db: PlayerTimeDB) {

    private val playerMap: ConcurrentMap<UUID, Player> = ConcurrentHashMap()

    /**
     * Adds the player to the playerMap.
     * It creates an new player if it's no persisted and adds/refreshes the join time
     *
     * @param uuid the actual uuid of a player
     * @param playerName for debugging because with the new api playerNames are not longer unique
     */
    fun addPlayer(uuid: UUID, playerName: String) {
        val player: Player = if (db.findByID(uuid)) {
            val player = db.readPlayer(uuid)
            player.joinTime = LocalDateTime.now()
            player
        } else {
            val player = Player(uuid, playerName)
            db.createPlayer(player)
            player
        }
        playerMap[uuid] = player
    }

    /**
     * Removes a player from the map and persists its status
     *
     *@param uuid players uuid
     */
    fun removePlayer(uuid: UUID) {
        persistPlayer(uuid)
        playerMap.remove(uuid)
    }

    /**
     * Wrapper for the map to prevent directly exposition
     *
     * @return status of the inner map
     */
    fun containsPlayer(uuid: UUID): Boolean {
        return playerMap.containsKey(uuid)
    }

    /**
     * Wrapper for the map to prevent directly exposition
     *
     * @return status of the inner map
     */
    fun isEmpty(): Boolean {
        return playerMap.isEmpty()
    }

    /**
     * Updates and writes the current status to the db
     *
     * @param uuid player uuid
     */
    fun persistPlayer(uuid: UUID) {
        val duration = timePlayed(uuid)
        val player = getPlayer(uuid)
        player.playTime = Duration.from(duration)
        player.lastSave = LocalDateTime.now()
        db.writePlayer(player)
    }

    /**
     * Persists all player
     */
    fun persistAllPlayer() {
        for ((uuid, _) in playerMap) {
            persistPlayer(uuid)
        }
    }

    /**
     * Returns the player with that uuid
     *
     * @param uuid player uuid
     */
    fun getPlayer(uuid: UUID): Player {
        return playerMap[uuid]!!
    }

    /**
     * Returns the playtime including the old time
     * if the stats got save it should use the save time instead of the join time
     *
     * @param uuid player uuid
     *
     * @return the total playtime
     */
    fun timePlayed(uuid: UUID): Duration {
        val player = getPlayer(uuid)
        val playTime: Duration = if ((player.lastSave != null) && Duration.between(player.lastSave, player.joinTime).isNegative) {
            Duration.between(player.lastSave, LocalDateTime.now())
        } else {
            Duration.between(player.joinTime, LocalDateTime.now())
        }
        return Duration.from(player.playTime).plus(playTime)
    }
}
