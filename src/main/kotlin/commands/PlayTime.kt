package commands

import PlayerService
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

/**
 * The command executor for the playtime
 *
 * @property playerService the service which holds the current status
 */
class PlayTime(private val playerService: PlayerService) : CommandExecutor {

    /**
     * It sends the playtime of and to the CommandSender
     *
     * @param sender to send a message
     *
     * @return true when the sender is a actual player
     */
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        return if (sender !is Player) {
            sender.sendMessage("This command can only be executed as a player")
            false
        } else {
            val duration = playerService.timePlayed(sender.uniqueId)

            val days: String =
                if (duration.toDaysPart() < 10) """0${duration.toDaysPart()}""" else """${duration.toDaysPart()}"""
            val hours: String =
                if (duration.toHoursPart() < 10) """0${duration.toHoursPart()}""" else """${duration.toHoursPart()}"""
            val minutes: String =
                if (duration.toMinutesPart() < 10) """0${duration.toMinutesPart()}""" else """${duration.toMinutesPart()}"""

            sender.sendMessage("""Your play time: $days:$hours:$minutes""")
            true
        }
    }
}
