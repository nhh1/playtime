#!/usr/bin/env bash

VERSION=0.1.5

sh ./gradlew shadowJar

cp build/libs/PlayTime-${VERSION}-SNAPSHOT-all.jar minecraftTestServer/plugins/

rm minecraftTestServer/plugins/PlayTime/*

cd minecraftTestServer/

java -Xmx2G -jar spigot-1.15.1.jar